Git criado para apresentação do serviço de integração e prospecção de recursos para ONG's chamado Iara, a Inteligencia Artificial de Relações Amplificadas.

Iara é uma API de chatbot para captação de recursos humanos e identificação de talentos.

Para executar a aplicação é necessario instalado a pilha LAMP, Linux, Apache, MySQL e PHP.

O usuário conversa diretamente com o chatbot Watson da IBM que via JSON faz uma requisição no arquivo /api/post.php.
O arquivo então alimenta o banco que dados, que pode ser consumido por qualquer API.

O arquivo /api/get.php, é o que consulta o banco e retorna o array com as entradas.
